basket.require({
  url: 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'
})
.then(function () {
  basket.require({
    url: 'https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js'
  })
  .then(function () {
    /* /sites/default/files/promotions/LANDINGURL/ */
    basket.require({
      url: 'scripts/main.min.js',
      skipCache: true
    })
  })
}, function (error) {
  console.log(error);
});
