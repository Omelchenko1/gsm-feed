$(function () {
  // AJAX-request for XML-feed

	$.ajax({
		type: "GET",
		url: 'https://r1.shop.kyivstar.ua/ru/public/94cae98d4b9104397a7cad9c039618e6/gsm_feed.xml',
		dataType: "xml",
		success: function (xml) {
			gadgetsList(xmlParser(xml));
		},
		error: function(jqXHR, textStatus, errorThrown){
			console.error(jqXHR);
			console.error(errorThrown);
		}
	}).done(function() {
		$('.gadget__img').load(function () {
			$('.gadget__img').addClass('gadget__img--loaded');
		});

		$('.gadgets__inner').slick({
			slidesToShow: 4,
			focusOnSelect: false
		});
	});

  function gadgetsList(data) {
		var LIMIT = 12;

		var gadgets = new Vue({
			el: '.gadgets',
			data: {
				gadgets: data,
				limit: LIMIT
			},

			methods: {
				// gadgetFocushandler: function(event) {
				// 	event.target.classList.add('gadget--focus');
				// },
				//
				// gadgetBlurhandler: function(event) {
				// 	event.target.classList.remove('gadget--focus');
				//
				// 	console.log(event.target)
				// }
			}
		});
	}

	// function for slicing xml CDATA element
	function sliceCDATA(elem) {
		var str = $(elem).html();
		return str.slice(9, (str.length - 3));
	}

	function xmlParser(xml) {
		var arr = []/*,
				xmlDoc = $.parseXML( xml ),
  			$xml = $( xmlDoc )*/;

		$(xml).find('offer').each(function () {
			var offer = {
				category: $(xml).find('category').attr('id'),
				model: sliceCDATA($(this).find('model')),
				picture: sliceCDATA($(this).find('picture')),
				url: sliceCDATA($(this).find('url')),
				price: $(this).find('price').text(),
				features: []
			};

			$(this).find('features').each(function(index) {
				if (index == 4) return false;

				offer.features.push($(this).attr('name') + ": " + $(this).text() );
			});

			arr.push(offer);
		});

		return arr;
	}
});
