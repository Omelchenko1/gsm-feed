 !(function() {
  var html = document.documentElement;

  if( sessionStorage.foutFontsLoaded ) {
    html.classList.add("fonts-loaded");
    return;
  } else {
    var script = document.createElement("script");
    script.async = true;
    script.src = "scripts/fontfaceobserver.min.js";

    script.onload = function () {
      var lightFont = new FontFaceObserver('MuseoSans', {
        weight: 300
      });
      var boldFont = new FontFaceObserver('MuseoSans', {
        weight: 700
      });

      Promise.all([
        lightFont.load(),
        boldFont.load()
      ]).then(function () {
        html.classList.add("fonts-loaded");

        sessionStorage.foutFontsLoaded = true;
      });
    }

    document.head.appendChild(script);
  }
})();
